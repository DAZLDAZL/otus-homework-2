﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private PromoDbContext context;

        public EfRepository(PromoDbContext context)
        {
            this.context = context;
        }

        public async Task AddAsync(T entity)
        {
            await context.AddAsync(entity);
            await context.SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<T> entities)
        {
            await context.AddRangeAsync(entities);
            await context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = await context.Set<T>().FindAsync(id);
            if (entity != null)
            {
                context.Set<T>().Remove(entity);
                await context.SaveChangesAsync();
            }
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var result = await context.Set<T>().ToArrayAsync();
            return result;
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            var result = await context.Set<T>().FindAsync(id);
            return result;
        }

        public async Task UpdateAsync(Guid id, T entity)
        {
            var ent = await context.Set<T>().FindAsync(id);
            if (ent != null)
            { 
               await context.SaveChangesAsync();
            }
        }
    }
}
