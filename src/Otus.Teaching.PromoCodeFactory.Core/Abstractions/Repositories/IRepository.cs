﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task AddAsync(T entity);

        Task DeleteAsync(Guid id);

        Task UpdateAsync(Guid id, T entity);

        Task AddRangeAsync(IEnumerable<T> entities);
    }
}