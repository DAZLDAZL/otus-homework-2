﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(25)]
        public string FirstName { get; set; }
        [MaxLength(25)]
        public string LastName { get; set; }

        [NotMapped]
        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(50)]
        public string Email { get; set; }

        public ICollection<PromoCode> PromoCodes { get; set; } = new List<PromoCode>();

        public ICollection<CustomerPreference> CustomerPreferences { get; set; } = new List<CustomerPreference>();
        //TODO: Списки Preferences и Promocodes 
    }
}