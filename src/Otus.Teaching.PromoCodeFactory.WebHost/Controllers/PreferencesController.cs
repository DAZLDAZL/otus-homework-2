﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    public class PreferencesController : ControllerBase
    {
        private IRepository<Preference> _preferencesRepository;
        public PreferencesController(IRepository<Preference> prefRepo)
        {
            _preferencesRepository= prefRepo;
        }

        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns>Массив со списком предпочтений</returns>
        [HttpGet]
        public async Task<ActionResult<PreferencesResponse>> GetPreferences()
        {
            var preferences = (await _preferencesRepository.GetAllAsync()).Select(x => new PreferencesResponse()
            {
             Id= x.Id,
             Name= x.Name
            }).ToList();
            return Ok(preferences);
        }
    }
}
