﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> customersRepo;
        private readonly IRepository<Preference> preferenceRepo;
        private readonly IRepository<PromoCode> promoCodeRepo;

        public CustomersController(IRepository<Customer> customersRepo, IRepository<PromoCode> promoCodeRepo,IRepository<Preference> preferencerepo)
        {
            this.customersRepo = customersRepo;
            this.promoCodeRepo = promoCodeRepo;
            preferenceRepo= preferencerepo;
        }
        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns>Возвращет список клиентов в сокращенном виде</returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var custs = await customersRepo.GetAllAsync();

            var response = custs.Select(x =>new CustomerShortResponse(x)).ToList();

            return Ok(response);
        }
        
        
        /// <summary>
        /// Получить клиента по его Id
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <returns>Возвращает представление клиента</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await customersRepo.GetByIdAsync(id);
            if (customer == null) return NotFound();
            CustomerResponse response = new CustomerResponse
            {
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                Id = customer.Id,

            };
            response.PromoCodes = customer.PromoCodes.Select(x => new PromoCodeShortResponse() { BeginDate = x.BeginDate.ToString(), Code = x.Code, EndDate = x.EndDate.ToString(), Id = x.Id, PartnerName = x.PartnerName, ServiceInfo = x.ServiceInfo }).ToList();
            var PrefIds = customer.CustomerPreferences.Select(x => x.Id).ToList();
            foreach (var prefId in PrefIds) {
                var pref = await preferenceRepo.GetByIdAsync(prefId);
                response.Preferences.Add(new PreferencesResponse() { Id = pref.Id, Name = pref.Name});
            }
            return response;
        }

        /// <summary>
        /// Создать запись о клиенте
        /// </summary>
        /// <param name="request">Представление клиента</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = new Customer() { FirstName = request.FirstName, LastName = request.LastName, Email = request.Email };
            await customersRepo.AddAsync(customer);
            foreach(var id in request.PreferenceIds)
            {
                if(await preferenceRepo.GetByIdAsync(id) != null)
                {
                    customer.CustomerPreferences.Add(new CustomerPreference { CustomerId = customer.Id,PreferenceId = id });
                }
            }
           await customersRepo.UpdateAsync(customer.Id,customer);
            return Ok();
        }
        

        /// <summary>
        /// Изменить запись о клиенте 
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <param name="request">Данные о клиенте</param>
        /// <returns>Возвращает 404, если запись не найдена. Возвращает 200 если запись найдена.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await customersRepo.GetByIdAsync(id);
            if (customer == null) return NotFound();

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
                   
            customer.CustomerPreferences.Clear();
            foreach (var i in request.PreferenceIds)
            {
                if (await preferenceRepo.GetByIdAsync(i) != null)
                {
                    customer.CustomerPreferences.Add(new CustomerPreference { CustomerId = customer.Id, PreferenceId = i });
                }
            }
            await customersRepo.UpdateAsync(customer.Id, customer);
            return Ok();
        }

        /// <summary>
        /// Удалить запись о клиенте
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <returns>Возвращает 404, если запись не найдена. Возвращает 200 если запись найдена.</returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var cust = await customersRepo.GetByIdAsync(id);
            if (cust == null) return NotFound();
            foreach (var code in cust.PromoCodes)
            {
                await promoCodeRepo.DeleteAsync(code.Id);
            }
            await customersRepo.DeleteAsync(id);
            return Ok();
        }
    }
}