﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private IRepository<PromoCode> _promocodesRepo;
        private IRepository<Customer> _customerRepo;
        private IRepository<Preference> _preferencesRepo;
        private IRepository<Employee> _employeeRepo;

        public PromocodesController(IRepository<PromoCode> promocodesRepo, IRepository<Customer> customerRepo,IRepository<Preference> prefs, IRepository<Employee> empls)
        {
            _promocodesRepo = promocodesRepo;
            _customerRepo = customerRepo;
            _preferencesRepo= prefs;
            _employeeRepo= empls;
        }


        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = (await _promocodesRepo.GetAllAsync()).Select(x => new PromoCodeShortResponse(){
             Id= x.Id,
             BeginDate = x.BeginDate.ToString(),
             EndDate= x.EndDate.ToString(),
             Code= x.Code,
             PartnerName= x.PartnerName,
             ServiceInfo= x.ServiceInfo,
            }).ToList();
            return Ok(promoCodes);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            PromoCode promoCode = new PromoCode()
            {
                Code = request.PromoCode,
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                Preference = await _preferencesRepo.GetByIdAsync(request.PreferenceId) ,
                PartnerManager = (await _employeeRepo.GetAllAsync()).FirstOrDefault(),
                Customer = (await _customerRepo.GetAllAsync()).FirstOrDefault(x => x.CustomerPreferences.Select(x=> x.PreferenceId).Contains(request.PreferenceId)),
            };
            await _promocodesRepo.AddAsync(promoCode);
            return Ok();
        }
    }
}