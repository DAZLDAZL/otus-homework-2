﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Extansions
{
    public static class SeedDataExtansion
    {
        public static void SeedDbData(this IApplicationBuilder app)
        {
            var context = app.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<PromoDbContext>();
            context.Employees.RemoveRange(context.Employees);
            context.Preferences.RemoveRange(context.Preferences);
            context.Customers.RemoveRange(context.Customers);
            context.Roles.RemoveRange(context.Roles);

            context.Roles.AddRange(FakeDataFactory.Roles);
            context.Preferences.AddRange(FakeDataFactory.Preferences);
            context.Customers.AddRange(FakeDataFactory.Customers);
            
            context.Employees.AddRange(new Core.Domain.Administration.Employee[]
            {
                new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Role = context.Roles.FirstOrDefault(x => x.Name == "Admin"),
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Role = context.Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                AppliedPromocodesCount = 10
            },
            });


            context.SaveChanges();
        }
    }
}
