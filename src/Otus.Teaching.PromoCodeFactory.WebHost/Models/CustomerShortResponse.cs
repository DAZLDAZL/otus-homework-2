﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CustomerShortResponse
    {
        public CustomerShortResponse() { }
        public CustomerShortResponse(Customer cust) {
        Id = cust.Id;
        FirstName= cust.FirstName;
            LastName= cust.LastName;
            Email= cust.Email;
        }
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
    }
}