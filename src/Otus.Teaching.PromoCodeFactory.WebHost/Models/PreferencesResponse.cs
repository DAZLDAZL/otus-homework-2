﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferencesResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
